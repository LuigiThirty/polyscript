#include "stdafx.h"
#include "Primitives.h"
#include "Evaluator.h"

#include <Windows.h>

#define DECLARE_PRIMITIVE_FN(NAME) static Object * NAME (Object *env, Object *list)

namespace PolyScript
{
	namespace Primitives
	{
		// Add a primitive function to the environment.
		void add_primitive(Object *env, const char *name, Primitive *fn) {
			Object *sym = Object::intern(name);
			Object *prim = Object::MakePrimitive(fn);
			Evaluator::add_variable(env, sym, prim);
		}

		// (+ <integer> ...)
		DECLARE_PRIMITIVE_FN(Plus) {
			
			bool promote_to_float = false;
			double sum = 0;

			for (Object *args = Evaluator::eval_list(env, list); args != Nil; args = args->cdr) {

				if (error_flag)
					return NULL;
				
				if(args->car->tag != T_ATOM)
					error("+ takes only numbers");

				if (args->car->atom_subtype == AT_INT)
					sum += args->car->int_value;
				else if (args->car->atom_subtype == AT_FLOAT)
				{
					sum += args->car->float_value;
					promote_to_float = true;
				}
			}

			if (promote_to_float)
				return Object::MakeFloat(sum);
			else
				return Object::MakeInt((int)sum);
		}

		// (- <integer> ...)
		DECLARE_PRIMITIVE_FN(Minus) {

			bool first_number = true;
			bool promote_to_float = false;
			double sum = 0;

			for (Object *args = Evaluator::eval_list(env, list); args != Nil; args = args->cdr) {

				if (error_flag)
					return NULL;

				if (args->car->tag != T_ATOM)
					error("- takes only numbers");

				if (first_number)
				{
					if (args->car->atom_subtype == AT_INT)
						sum += args->car->int_value;
					else if (args->car->atom_subtype == AT_FLOAT)
					{
						sum += args->car->float_value;
						promote_to_float = true;
					}

					first_number = false;
				}

				else {
					if (args->car->atom_subtype == AT_INT)
						sum -= args->car->int_value;
					else if (args->car->atom_subtype == AT_FLOAT)
					{
						sum -= args->car->float_value;
						promote_to_float = true;
					}
				}

			}

			if (promote_to_float)
				return Object::MakeFloat(sum);
			else
				return Object::MakeInt((int)sum);
		}

		// (- <integer> ...)
		Object *Multiply(Object *env, Object *list) {

			bool promote_to_float = false;
			double sum = 0;

			for (Object *args = Evaluator::eval_list(env, list); args != Nil; args = args->cdr) {

				if (args->car->tag != T_ATOM)
					error("- takes only numbers");

				else {
					if (args->car->atom_subtype == AT_INT)
						sum *= args->car->int_value;
					else if (args->car->atom_subtype == AT_FLOAT)
					{
						sum *= args->car->float_value;
						promote_to_float = true;
					}
				}

			}

			if (!error_flag)
			{
				if (promote_to_float)
					return Object::MakeFloat(sum);
				else if (!error_flag)
					return Object::MakeInt((int)sum);
			}
			else
				return NULL;
		}

		bool look_for_comma(Object *env, Object *obj)
		{
			char c[100];
			sprintf(c, "Looking for comma in object %p\n", obj);
			OutputDebugStringA(c);

			if(obj->tag == T_CELL)
			{
				if(look_for_comma(env, obj->car))
				{
					sprintf(c, "Found a comma!\n", obj);
					OutputDebugStringA(c);

					// Replace the next element with the variable bound to that symbol.
					Object *symbol = Evaluator::find(env, obj->cdr->car);
					Object *value = Evaluator::eval(env, symbol->car);

					// Unlink the comma.
					obj->car = obj->cdr->car; 
					obj->cdr = obj->cdr->cdr;

					Object *new_var = Object::alloc(value->tag, value->size);
					obj->car = new_var;
					
					// Replace current->car with the value.
					obj->car->CopyData(value);
				}

				look_for_comma(env, obj->cdr);
			}
			else
			{
				if(obj->tag == T_SPECIAL && obj->special_subtype == T_COMMA)
				{
					return true;
				}
				return false;
			}
		}

		// 'expr
		DECLARE_PRIMITIVE_FN(Quote) {
			if (Evaluator::list_length(list) != 1)
			{
				error("Malformed quote");
				return NULL;
			}

			Object *current = list->car;
			Object *parent = current;

			if(current->tag == T_CELL)
			{
				look_for_comma(env, current);
			}

				/*
				if(current->car->special_subtype == T_COMMA)
				{
					Object *comma = current->car;

					// Replace the next element with the variable bound to that symbol.
					Object *symbol = Evaluator::find(env, current->cdr->car);
					Object *value = Evaluator::eval(env, symbol->car);

					// Unlink the comma.
					current->car = current->cdr->car; 
					current->cdr = current->cdr->cdr;
					
					// Replace current->car with the value.
					current->car->CopyData(value);
				}
				else
				{
					if(current->tag == T_CELL)
					{
						current = current->cdr;
					}
					else
					{
						done = true;
					}
				}

  }
				*/
				
			return list->car;
		}

		// (list expr ...)
		DECLARE_PRIMITIVE_FN(List) {
			return Evaluator::eval_list(env, list);
		}

		// (defun <symbol> (<symbol> ...) expr ...)
		DECLARE_PRIMITIVE_FN(Defun) {
			return Evaluator::handle_defun(env, list, T_FUNCTION);
		}

		// (lambda (<symbol> ...) expr ...)
		DECLARE_PRIMITIVE_FN(Lambda) {
			return Evaluator::handle_function(env, list, T_FUNCTION);
		}

		// (setq <symbol> expr)
		DECLARE_PRIMITIVE_FN(Setq) {
			if (Evaluator::list_length(list) != 2 || (list->car->tag != T_ATOM && list->car->atom_subtype == AT_SYMBOL))
			{
				error("Malformed setq");
				return NULL;
			}
				
			Object *bind = Evaluator::find(env, list->car);
			if (!bind)
			{
				error("Unbound variable %s", list->car->name);
				return NULL;
			}
				
			Object *value = Evaluator::eval(env, list->cdr->car);
			bind->cdr = value;
			return value;
		}

		// (define <symbol> expr)
		DECLARE_PRIMITIVE_FN(Define)
		{
			if (Evaluator::list_length(list) != 2 || (list->car->tag != T_ATOM && list->car->atom_subtype == AT_SYMBOL))
			{
				error("Malformed setq");
				return NULL;
			}
			Object *sym = Evaluator::eval(env, list->car);
			Object *value = Evaluator::eval(env, list->cdr->car);
			Evaluator::add_variable(env, sym, value);
			return sym;
		}

		// (defmacro <symbol> (<symbol> ...) expr ...)
		DECLARE_PRIMITIVE_FN(Defmacro)
		{
			return Evaluator::handle_defun(env, list, T_MACRO);
		}

		// (println expr)
		DECLARE_PRIMITIVE_FN(Println) 
		{
			Parser::print(Evaluator::eval(env, list->car), Parser::PD_DEBUGOUTPUT);
			OutputDebugStringA("\n");
			return Nil;
		}

		DECLARE_PRIMITIVE_FN(Eq)
		{
			if (Evaluator::list_length(list) != 2)
			{
				error(" Malformed =");
				return NULL;
			}
				

			Object *values = Evaluator::eval_list(env, list);
			
			Object *x = values->car;
			Object *y = values->cdr->car;

			if (x->tag != y->tag)
			{
				error("eq cannot evaluate equality of two different object types");
				return Nil;
			}

			if (x->tag == T_ATOM && y->tag == T_ATOM)
			{
				switch (x->atom_subtype)
				{
				case AT_INT:
				case AT_FLOAT:
					return x->int_value == y->int_value ? True : Nil;
				case AT_SYMBOL:
					return x->name == y->name ? True : Nil;
				default:
					// TODO: other atom types
					error("Only numerical and symbol equality is currently supported by eq");
					return Nil;
				}
			}

			return NULL;
		}

		DECLARE_PRIMITIVE_FN(GreaterThan)
		{
			if (Evaluator::list_length(list) != 2)
			{
				error("Malformed <=");
				return NULL;
			}

			Object *values = Evaluator::eval_list(env, list);

			Object *x = values->car;
			Object *y = values->cdr->car;

			if (x->tag != y->tag)
			{
				error("eq cannot evaluate equality of two different object types");
				return Nil;
			}

			if (x->tag == T_ATOM && y->tag == T_ATOM)
			{
				switch (x->atom_subtype)
				{
				case AT_INT:
				case AT_FLOAT:
					return x->int_value > y->int_value ? True : Nil;
				case AT_SYMBOL:
					return x->name > y->name ? True : Nil;
				}
			}
			else
			{
				error("Only numerical and symbol equality is currently supported by eq");
				return Nil;
			}

			return NULL;
		}

		DECLARE_PRIMITIVE_FN(GreaterThanOrEqual)
		{
			if (Evaluator::list_length(list) != 2)
			{
				error("Malformed <=");
				return NULL;
			}

			Object *values = Evaluator::eval_list(env, list);

			Object *x = values->car;
			Object *y = values->cdr->car;

			if (x->tag != y->tag)
			{
				error("eq cannot evaluate equality of two different object types");
				return Nil;
			}

			if (x->tag == T_ATOM && y->tag == T_ATOM)
			{
				switch (x->atom_subtype)
				{
				case AT_INT:
				case AT_FLOAT:
					return x->int_value >= y->int_value ? True : Nil;
				case AT_SYMBOL:
					return x->name >= y->name ? True : Nil;
				}
			}
			else
			{
				error("Only numerical and symbol equality is currently supported by eq");
				return Nil;
			}

			return NULL;
		}

		DECLARE_PRIMITIVE_FN(LessThan)
		{
			if (Evaluator::list_length(list) != 2)
			{
				error("Malformed <=");
				return NULL;
			}

			Object *values = Evaluator::eval_list(env, list);

			Object *x = values->car;
			Object *y = values->cdr->car;

			if (x->tag != y->tag)
			{
				error("eq cannot evaluate equality of two different object types");
				return Nil;
			}

			if (x->tag == T_ATOM && y->tag == T_ATOM)
			{
				switch (x->atom_subtype)
				{
				case AT_INT:
				case AT_FLOAT:
					return x->int_value < y->int_value ? True : Nil;
				case AT_SYMBOL:
					return x->name < y->name ? True : Nil;
				}
			}
			else
			{
				error("Only numerical and symbol equality is currently supported by eq");
				return Nil;
			}

			return NULL;
		}

		DECLARE_PRIMITIVE_FN(Progn)
		{
			// Progn - Evaluate each form in turn, returning the last form's value.

			int length = Evaluator::list_length(list);
			if (length == 0)
			{
				error("Malformed PROGN");
				return NULL;
			}

			Object *result;
			for(int i=0; i<length; i++)
			{
				result = Evaluator::eval_list_element(env, list, i);
			}

			return result; // Returns the last evaluated form.
		}

		DECLARE_PRIMITIVE_FN(LessThanOrEqual)
		{
			if (Evaluator::list_length(list) != 2)
			{
				error("Malformed <=");
				return NULL;
			}

			Object *values = Evaluator::eval_list(env, list);

			Object *x = values->car;
			Object *y = values->cdr->car;

			if (x->tag != y->tag)
			{
				error("eq cannot evaluate equality of two different object types");
				return Nil;
			}

			if (x->tag == T_ATOM && y->tag == T_ATOM)
			{
				switch (x->atom_subtype)
				{
				case AT_INT:
				case AT_FLOAT:
					return x->int_value <= y->int_value ? True : Nil;
				case AT_SYMBOL:
					return x->name <= y->name ? True : Nil;
				}
			}
			else
			{
				error("Only numerical and symbol equality is currently supported by eq");
				return Nil;
			}

			return NULL;
		}

		// Predicates
		DECLARE_PRIMITIVE_FN(SymbolP)
		{
			Object *values = Evaluator::eval_list(env, list);

			if (Evaluator::list_length(list) != 1)
			{
				error("Too many arguments: found %d instead of 1", Evaluator::list_length(list));
				return NULL;
			}

			Object *val = values->car;
			if (val->IsAtomSubtype(AT_SYMBOL))
				return True;
			else
				return Nil;
		}

		DECLARE_PRIMITIVE_FN(AtomP)
		{
			Object *values = Evaluator::eval_list(env, list);

			if (Evaluator::list_length(list) != 1)
			{
				error("Too many arguments: found %d instead of 1", Evaluator::list_length(list));
				return NULL;
			}

			Object *val = values->car;
			if (val->tag == T_ATOM)
				return True;
			else
				return Nil;
		}

		DECLARE_PRIMITIVE_FN(ConsP)
		{
			Object *values = Evaluator::eval_list(env, list);

			if (Evaluator::list_length(list) != 1)
			{
				error("Too many arguments: found %d instead of 1", Evaluator::list_length(list));
				return NULL;
			}

			Object *val = values->car;
			if (val->tag == T_CELL)
				return True;
			else
				return Nil;
		}

		DECLARE_PRIMITIVE_FN(NumberP)
		{
			Object *values = Evaluator::eval_list(env, list);

			if (Evaluator::list_length(list) != 1)
			{
				error("Too many arguments: found %d instead of 1", Evaluator::list_length(list));
				return NULL;
			}

			Object *val = values->car;
			if (val->IsAtomSubtype(AT_FLOAT) || val->IsAtomSubtype(AT_INT))
				return True;
			else
				return Nil;
		}

		DECLARE_PRIMITIVE_FN(FloatP)
		{
			Object *values = Evaluator::eval_list(env, list);

			if (Evaluator::list_length(list) != 1)
			{
				error("Too many arguments: found %d instead of 1", Evaluator::list_length(list));
				return NULL;
			}

			Object *val = values->car;
			if (val->IsAtomSubtype(AT_FLOAT))
				return True;
			else
				return Nil;
		}

		DECLARE_PRIMITIVE_FN(IntegerP)
		{
			Object *values = Evaluator::eval_list(env, list);

			if (Evaluator::list_length(list) != 1)
			{
				error("Too many arguments: found %d instead of 1", Evaluator::list_length(list));
				return NULL;
			}

			Object *val = values->car;
			if (val->IsAtomSubtype(AT_INT))
				return True;
			else
				return Nil;
		}

		DECLARE_PRIMITIVE_FN(ZeroP)
		{
			Object *values = Evaluator::eval_list(env, list);

			if (Evaluator::list_length(list) != 1)
			{
				error("Too many arguments: found %d instead of 1", Evaluator::list_length(list));
				return NULL;
			}

			Object *val = values->car;
			if (!val->IsNumber())
			{
				error("Argument is not a number");
				return NULL;
			}
			else if (val->IsAtomSubtype(AT_INT) && val->int_value == 0)
				return True;
			else if (val->IsAtomSubtype(AT_FLOAT) && val->float_value == 0)
				return True;
			else 
				return Nil;
		}

		DECLARE_PRIMITIVE_FN(PlusP)
		{
			Object *values = Evaluator::eval_list(env, list);

			if (Evaluator::list_length(list) != 1)
			{
				error("Too many arguments: found %d instead of 1", Evaluator::list_length(list));
				return NULL;
			}

			Object *val = values->car;
			if (!val->IsNumber())
			{
				error("Argument is not a number");
				return NULL;
			}
			else if (val->IsAtomSubtype(AT_INT) && val->int_value > 0)
				return True;
			else if (val->IsAtomSubtype(AT_FLOAT) && val->float_value > 0)
				return True;
			else
				return Nil;
		}

		DECLARE_PRIMITIVE_FN(MinusP)
		{
			Object *values = Evaluator::eval_list(env, list);

			if (Evaluator::list_length(list) != 1)
			{
				error("Too many arguments: found %d instead of 1", Evaluator::list_length(list));
				return NULL;
			}

			Object *val = values->car;
			if (!val->IsNumber())
			{
				error("Argument is not a number");
				return NULL;
			}
			else if (val->IsAtomSubtype(AT_INT) && val->int_value < 0)
				return True;
			else if (val->IsAtomSubtype(AT_FLOAT) && val->float_value < 0)
				return True;
			else
				return Nil;
		}

		DECLARE_PRIMITIVE_FN(If)
		{
			// Evaluate the first form.
			// If the first form is not nil, evaluate the second form.
			// If the first form is nil, evaluate the third form if one exists.

			int elements = Evaluator::list_length(list);

			if (elements != 2 && elements != 3)
			{
				error("Incorrect number of arguments: found %d instead of 1", Evaluator::list_length(list));
				return NULL;
			}

			// Evaluate the first form.
			Object *first_form = Evaluator::eval_list_element(env, list, 0);

			if (first_form->car->special_subtype == T_TRUE)
			{
				return Evaluator::eval_list_element(env, list, 1)->car;
			}
			else if (first_form->car->special_subtype == T_NIL)
			{
				if (elements == 3)
					return Evaluator::eval_list_element(env, list, 2)->car;
				else
					return Nil;
			}
		}

		DECLARE_PRIMITIVE_FN(ReadChar)
		{
			int elements = Evaluator::list_length(list);
			if (elements > 1)
			{
				error("Incorrect number of arguments: found %d instead of 1", Evaluator::list_length(list));
				return NULL;
			}

			Object *obj = Object::MakeCharacter(Parser::get_next_char());

			return obj;

		}

		DECLARE_PRIMITIVE_FN(SequenceLength)
		{
			int elements = Evaluator::list_length(list);
			if (elements == 1)
			{
				// Evaluate the form.
				Object *form = Evaluator::eval_list(env, list);

				if(form->car->tag == T_ATOM && form->car->IsSequence())
				{
					switch (form->car->atom_subtype)
					{
					case AT_STRING:
						return Object::MakeInt(strlen(form->car->str_value));
					case AT_ARRAY:
						return Object::MakeInt(form->car->array_length);
					default:
						error("Not a sequence");
						return NULL;
					}
				}
				else
				{
					error("Not a sequence");
					return NULL;
				}
			}
		}

		DECLARE_PRIMITIVE_FN(SequenceElementAt)
		{
			int elements = Evaluator::list_length(list);
			if (elements == 2)
			{
				// Evaluate the form.
				Object *form = Evaluator::eval_list(env, list);

				if(form->car->tag == T_ATOM && form->car->IsSequence())
				{
					switch (form->car->atom_subtype)
					{
					case AT_STRING:
						return Object::MakeCharacter(form->car->str_value[form->cdr->car->int_value]);
					default:
						error("Not a sequence or sequence type not yet supported");
						return NULL;
					}
				}
				else
				{
					error("Not a sequence");
					return NULL;
				}
			}
			else
			{
				error("Too many arguments to elt, expected 2");
				return NULL;
			}
		}

		DECLARE_PRIMITIVE_FN(MakeVector)
		{
			int elements = Evaluator::list_length(list);
			Object *vector = Object::MakeArray(elements);

			// Fill the vector with supplied objects.
			
			// Evaluate all forms in turn, add them to the vector.
			for(int i=0; i<elements; i++)
			{
				Object *form = Evaluator::eval_list_element(env, list, i);
				vector->array_contents[i] = form->car;
			}


			return vector;
		}

		DECLARE_PRIMITIVE_FN(TypeOf)
		{
			int elements = Evaluator::list_length(list);
			if(elements != 1)
			{
				error("Unexpected number of arguments: %d (expected 1)", elements);
				return NULL;
			}

			Object *o = Evaluator::eval_list_element(env, list, 0);
			return o->car->get_type_symbol();
		}

		DECLARE_PRIMITIVE_FN(CTypeTag)
		{
			int elements = Evaluator::list_length(list);
			if(elements != 1)
			{
				error("Unexpected number of arguments: %d (expected 1)", elements);
				return NULL;
			}

			Object *o = Evaluator::eval_list_element(env, list, 0);
			return Object::cons(Object::MakeInt(o->car->tag), Object::MakeInt(o->car->atom_subtype));
		}

		DECLARE_PRIMITIVE_FN(TypeP)
		{
			int elements = Evaluator::list_length(list);
			if(elements != 2)
			{
				error("Unexpected number of arguments: %d (expected 2)", elements);
				return NULL;
			}

			Object *a = Evaluator::eval_list_element(env, list, 0);
			Object *b = Evaluator::eval_list_element(env, list, 1);

			if(!b->car->IsSymbol())
			{
				error("Second argument is not a SYMBOL");
				return NULL;
			}

			Object *l = Object::cons(a->car->get_type_symbol(), b);
			
			// Two symbols are eq if they have the same print value.
			if(strcmp(l->car->name, b->car->name) == 0)
			{
				return PolyScript::True;
			}
			else
			{
				return PolyScript::Nil;
			}

		}

		DECLARE_PRIMITIVE_FN(Let)
		{
			// Create a new env for everything in this function.

			int elements = Evaluator::list_length(list);
			if(elements < 2)
			{
				error("Unexpected number of arguments: %d (expected at least 2)", elements);
				return NULL;
			}

			Object *local_env = Object::MakeEnv(Nil, env);

			int var_count = Evaluator::list_length(list->car);

			Object *variables = list->car;

			for(int i=0; i<var_count; i++)
			{
				// First element of variable is the new symbol's name
				Object *element_symbol = variables->car->car;
				Object *element_value = variables->car->cdr->car;
				Evaluator::add_variable(local_env, element_symbol, element_value);

				variables = variables->cdr;
			}

			Object *result;
			for(int j=1; j<elements; j++)
			{
				result = Evaluator::eval_list_element(local_env, list, j);
			}

			delete local_env;

			return result->car;
		}

		DECLARE_PRIMITIVE_FN(Car)
		{
			int elements = Evaluator::list_length(list);
			if(elements != 1)
			{
				error("car: Unexpected number of arguments: %d", elements);
				return NULL;
			}

			return Evaluator::eval_list_element(env, list, 0)->car->car;
		}

		DECLARE_PRIMITIVE_FN(Cdr)
		{
			int elements = Evaluator::list_length(list);
			if(elements != 1)
			{
				error("cdr: Unexpected number of arguments: %d", elements);
				return NULL;
			}

			return Evaluator::eval_list_element(env, list, 0)->car->cdr;
		}

		DECLARE_PRIMITIVE_FN(Intern)
		{
			int elements = Evaluator::list_length(list);
			if(elements != 1)
			{
				error("Intern: Unexpected number of arguments: %d", elements);
				return NULL;
			}

			Object *symbol_name = Evaluator::eval_list_element(env, list, 0);
			return Object::intern(symbol_name->car->str_value);
		}

		DECLARE_PRIMITIVE_FN(Concatenate)
		{
			int elements = Evaluator::list_length(list);
			if(elements < 2)
			{
				error("Concatenate: Unexpected number of arguments: %d", elements);
				return NULL;
			}

			// Concatenate all sequences that follow.

			// Get type specifier.
			Object *type_specifier = Evaluator::eval_list_element(env, list, 0);

			// TODO: handle type specifier, for now assume strings
			char str[1024];
			int ptr = 0;

			for(int i=1; i<elements; i++)
			{
				Object *o = Evaluator::eval_list_element(env, list, i);
				if(o->car->tag != T_ATOM && o->car->atom_subtype != AT_STRING)
				{
					error("Concatenate: Argument is not a string");
					return 0;
				}

				memcpy(&(str[ptr]), o->car->str_value, strlen(o->car->str_value));
				ptr += strlen(o->car->str_value);
				
			}

			str[ptr] = 0;
			return Object::MakeString(str);
		}

		///////////

		// Add all our primitives to the environment.
		void create_primitives(Object *env)
		{
			add_primitive(env, "car", Car);
			add_primitive(env, "cdr", Cdr);
			
			// Mathematical primitives
			add_primitive(env, "plus", Plus);
			add_primitive(env, "minus", Minus);
			add_primitive(env, "multiply", Multiply);

			// Lisp primitives
			add_primitive(env, "intern", Intern);
			add_primitive(env, "let", Let);
			add_primitive(env, "quote", Quote);
			add_primitive(env, "list", List);
			add_primitive(env, "defun", Defun);
			add_primitive(env, "lambda", Lambda);
			add_primitive(env, "setq", Setq);
			add_primitive(env, "define", Define);
			add_primitive(env, "defmacro", Defmacro);
			add_primitive(env, "progn", Progn);
			add_primitive(env, "println", Println);
			add_primitive(env, "type-of", TypeOf);
			add_primitive(env, "c-type-tag", CTypeTag);

			// Equality primitives
			add_primitive(env, "eq", Eq);
			add_primitive(env, ">", GreaterThan);
			add_primitive(env, "<", LessThan);
			add_primitive(env, ">=", GreaterThanOrEqual);
			add_primitive(env, "<=", LessThanOrEqual);

			// Some predicates
			add_primitive(env, "numberp", NumberP);
			add_primitive(env, "floatp", FloatP);
			add_primitive(env, "integerp", IntegerP);
			add_primitive(env, "zerop", ZeroP);
			add_primitive(env, "plusp", PlusP);
			add_primitive(env, "minusp", MinusP);
			add_primitive(env, "typep", TypeP);

			add_primitive(env, "if", If);

			add_primitive(env, "read-char", ReadChar);
			
			// Sequence primitives
			add_primitive(env, "vector", MakeVector);
			add_primitive(env, "length", SequenceLength);
			add_primitive(env, "elt", SequenceElementAt);
			add_primitive(env, "concatenate", Concatenate);
			
		}
	};
};
