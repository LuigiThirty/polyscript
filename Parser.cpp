#include "stdafx.h"
#include "Parser.h"

#include "PrintObject.h"

namespace PolyScript
{
	namespace Parser
	{
		bool inside_backtick = true;

		wchar_t output_buffer[512];
		char output_buffer_ansi[512];

		int peek()
		{
			if (PolyScript::evaluating_a_script)
			{
				return PolyScript::string_under_evaluation[PolyScript::string_pointer];
			}
			else 
			{
				int c = getchar();
				ungetc(c, stdin);
				return c;
			}

		}

		char get_next_char()
		{
			if (PolyScript::evaluating_a_script)
			{
				return PolyScript::string_under_evaluation[PolyScript::string_pointer++];
			}
			else
			{
				return getchar();
			}
		}

		// Skips the input until newline is found. Newline is one of \r, \r\n or \n.
		void skip_line(void) {
			for (;;) {
				int c = get_next_char();
				if (c == EOF || c == '\n' || c == '\0')
					return;
				if (c == '\r') {
					if (peek() == '\n')
						get_next_char();
					return;
				}
			}
		}

		int read_number(int val) {
			while (isdigit(peek()))
				val = val * 10 + (get_next_char() - '0');
			return val;
		}

		Object * read_string()
		{
			// read until we find another "
			char buf[STRING_MAX_LEN];
			int len = 0;

			while (peek() != '"' && peek() != '\0' && peek() != '\r' && peek() != '\n')
			{
				if (len >= STRING_MAX_LEN)
				{
					error("String too long");
					return NULL;
				}

				buf[len++] = get_next_char();
			}

			buf[len++] = '\0';

			// dispose of ending "
			if (peek() == '"')
				get_next_char();

			return Object::MakeString(buf);
		}

		Object * read_numeric_string(char c)
		{
			// parse the string and figure out if it's a float or an int.
			char buf[NUMBER_MAX_LEN + 1];
			int len = 1;
			buf[0] = c;

			bool decimal_flag = false;
			bool negative_flag = false;

			while (isdigit(peek()) || peek() == '.' || peek() == '-')
			{
				if (len >= NUMBER_MAX_LEN)
					error("Number too long");

				if (peek() == '.' && decimal_flag == true)
					error("Invalid numeric value: two decimal points");
				else if (peek() == '.')
					decimal_flag = true;

				if (peek() == '-' && negative_flag == true)
					error("Invalid numeric value: two negative signs");
				else if (peek() == '-')
					negative_flag = true;

				buf[len++] = get_next_char();
			}

			if (decimal_flag)
				return Object::MakeFloat(atof(buf));
			else
				return Object::MakeInt(atoi(buf));
		}

		Object *read_symbol(char c) {
			char buf[SYMBOL_MAX_LEN + 1];
			int len = 1;
			buf[0] = c;
			while (isalnum(peek()) || peek() == '-' || peek() == '=') {
				if (SYMBOL_MAX_LEN <= len)
					error("Symbol name too long");
				buf[len++] = toupper(get_next_char());
			}
			buf[len] = '\0';
			return Object::intern(buf);
		}

		Object *read_pound_sign(char c)
		{
			// Read something prepended by a pound sign.
			// Character, vector, something...
			char buf[STRING_MAX_LEN];
			int len = 0;

			char p = peek();
			if(peek() == '\\')
			{
				get_next_char();
				while(isalnum(peek()))
				{
					buf[len++] = get_next_char();
					
				}
				buf[len] = '\0';

				if(len == 1)
					return Object::MakeCharacter(buf[0]);
				else
				{
					error("Illegal character literal");
					return NULL;
				}
			}

			else
			{
				error("Unknown character sigil");
				return NULL;
			}
		}

		// Reader macro ' (single quote).
		// It reads an expression and returns (quote <expr>).
		Object *read_quote(void) {
			Object *sym = Object::intern("quote");
			return Object::cons(sym, Object::cons(read(inside_backtick), Nil));
		}

		// Reads a list. Note that '(' has already been read.
		Object *read_list(bool in_backtick) {
			if (error_flag)
				return NULL;

			Object *obj = read(in_backtick);
			if (!obj)
				error("Unclosed parenthesis");
			if (obj == Dot)
				error("Stray dot");
			if (obj == Cparen)
				return Nil;
			Object *head, *tail;
			head = tail = Object::cons(obj, Nil);

			for (;;) {
				Object *obj = read(in_backtick);
				if (!obj)
					error("Unclosed parenthesis");
				if (obj == Cparen)
					return head;
				if (obj == Dot) {
					tail->cdr = read(in_backtick);
					if (read(in_backtick) != Cparen)
						error("Closed parenthesis expected after dot");
					return head;
				}
				tail->cdr = Object::cons(obj, Nil);
				tail = tail->cdr;
			}
		}

		Object *read_backtick(char c)
		{
			// Backtick inverts quoting.
			// Anything THAT IS NOT preceded by a comma is quoted.
			// Anything THAT IS preceded by a comma is evaluated.
			//
			// A backtick not inside a macro is an error.

			Object *backticked = read_quote();
			return backticked;
		}

		Object *read_comma(char c)
		{
			//return Object::cons(Comma, read(true));
			return PolyScript::Comma;
		}

		// Reads a line from the console.
		Object *read(bool in_backtick) {
			for (;;) {
				if (error_flag)
					return NULL;

				unsigned char c = get_next_char();

				if (c == ' ' || c == '\n' || c == '\r' || c == '\t')
					continue;
				else if (c == EOF || c == '\0' || c == 0xCC)
				{
					end_of_script_flag = true;
					return NULL;
				}
				else if (c == ';')
				{
					skip_line();
					continue;
				}
				else if (c == '(')
					return read_list(in_backtick);
				else if (c == ')')
					return Cparen;
				else if (c == '.')
					return Dot;
				else if (c == '"')
					return read_string();
				else if (c == '\'')
					return read_quote();
				else if (isdigit(c) || c == '-')
					return read_numeric_string(c);
				else if (c == '`')
					return read_backtick(c);
				else if (c == ',')
					return read_comma(c);
				else if (c == '#')
					return read_pound_sign(c);
				else if (isalpha(c) || strchr("+><=!@$%^&*", c))
					return read_symbol(c);
				else
					error("Don't know how to handle %c", c);
			}
		}

		// Prints the given object.
		void print(Object *obj, PrintDestination destination) {
			if (error_flag)
				return;

			switch (obj->tag) {

			case T_ATOM:
				switch (obj->atom_subtype)
				{
				case AT_INT:
					print_integer(obj, destination);
					return;
				case AT_FLOAT:
					print_float(obj, destination);
					return;
				case AT_SYMBOL:
					print_symbol(obj, destination);
					return;
				case AT_STRING:
					print_string(obj, destination);
					return;
				case AT_CHARACTER:
					print_character(obj, destination);
					return;
				case AT_ARRAY:
					print_array(obj, destination);
					return;
				}

			case T_CELL:
				print_cell(obj, destination);
				return;
			case T_PRIMITIVE:
				print_primitive(obj, destination);
				return;
			case T_FUNCTION:
				print_function(obj, destination);
				return;
			case T_MACRO:
				print_macro(obj, destination);
				return;
			case T_SPECIAL:
				print_special(obj, destination);
				return;
			default:
				error("Bug: print: Unknown tag type: %d", obj->tag);
				return;
			}
		}
	}
}