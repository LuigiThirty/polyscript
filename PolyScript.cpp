// PolyScript.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "PolyScript.h"
#include "Primitives.h"
#include "Parser.h"
#include "Evaluator.h"

namespace PolyScript
{
	Object * obarray;

	Object * Nil;
	Object * Dot;
	Object * Cparen;
	Object * True;
	Object * Backtick;
	Object * Comma;

	Object * env;

	bool error_flag;

	char string_under_evaluation[65536];
	int string_pointer = 0;
	bool evaluating_a_script = false;
	bool end_of_script_flag = false;

	void error(const char *fmt, ...) {
		char output_buffer[512];
		va_list ap;
		va_start(ap, fmt);
		vsprintf(output_buffer, fmt, ap);
		va_end(ap);
		OutputDebugStringA(output_buffer);
		//exit(1);

		error_flag = true;
	}

	void create_symbols(Object *env)
	{
		// Create type symbols.
		Object::intern("ARRAY");
		Object::intern("ATOM");
		Object::intern("CHARACTER");
		Object::intern("CONS");
		Object::intern("FLOAT");
		Object::intern("FUNCTION");
		Object::intern("INTEGER");
		Object::intern("LIST");
		Object::intern("NIL");
		Object::intern("NULL");
		Object::intern("NUMBER");
		Object::intern("SEQUENCE");
		Object::intern("STRING");
		Object::intern("SYMBOL");
		Object::intern("T");
		Object::intern("VECTOR");
	}

	void Initialize()
	{
		Nil = Object::MakeSpecial(T_NIL);
		Dot = Object::MakeSpecial(T_DOT);
		Cparen = Object::MakeSpecial(T_CPAREN);
		True = Object::MakeSpecial(T_TRUE);
		Backtick = Object::MakeSpecial(T_BACKTICK);
		Comma = Object::MakeSpecial(T_COMMA);

		obarray = Nil;

		env = Object::MakeEnv(Nil, NULL);

		Primitives::create_primitives(env);
		create_symbols(env);
	}

	void EvaluateString(const char *line)
	{
		evaluating_a_script = true;
		strcpy(string_under_evaluation, line);

		Object *expr = Parser::read(false);
		Parser::print(Evaluator::eval(env, expr), Parser::PD_DEBUGOUTPUT);
		OutputDebugStringW(L"\n");

		evaluating_a_script = false;
	}

	void EvaluateFile(const char *path)
	{
		// Evaluate each statement in the file.
		HANDLE hScriptFile;
		char scriptBuffer[16384];
		DWORD dwBytesRead;

		hScriptFile = CreateFile(path,
			GENERIC_READ,
			FILE_SHARE_READ,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			NULL);

		// TODO: This will only read 16KB of a file.
		ReadFile(hScriptFile, scriptBuffer, 16384, &dwBytesRead, NULL);
		memset(string_under_evaluation, 0, 65536);

		evaluating_a_script = true;
		end_of_script_flag = false;

		strcpy(string_under_evaluation, scriptBuffer);

		Object *expr = Parser::read(false);

		while(expr != NULL)
		{
			Parser::print(Evaluator::eval(env, expr), Parser::PD_DEBUGOUTPUT);
			OutputDebugStringA("\n*\n");
			if(!end_of_script_flag)
			{
				expr = Parser::read(false);
			}			
		}

		evaluating_a_script = false;
	}
}

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}


// This is an example of an exported variable
POLYSCRIPT_API int nPolyScript=0;

// This is an example of an exported function.
POLYSCRIPT_API int fnPolyScript(void)
{
	return 42;
}

// This is the constructor of a class that has been exported.
// see PolyScript.h for the class definition
CPolyScript::CPolyScript()
{ 
	return; 
}


/*
int main()
{
	PolyScript::Initialize();

	//PolyScript::EvaluateString("(if (eq 4 4) (plus 2 2))");
	//printf("\n");

	while (true)
	{
		PolyScript::error_flag = false;

		printf("> ");
		PolyScript::Object *expr = PolyScript::Parser::read();
		if(!PolyScript::error_flag)
			PolyScript::Parser::print(PolyScript::Evaluator::eval(PolyScript::env, expr));
		printf("\n");

		//PolyScript::Parser::win_debug_print(PolyScript::Evaluator::eval(PolyScript::env, expr));
		//OutputDebugStringW(L"\n");
		
	}

    return 0;
}*/


