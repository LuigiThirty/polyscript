#include <cstring>

namespace PolyScript
{
	namespace Parser
	{
		extern wchar_t output_buffer[512];
		extern char output_buffer_ansi[512];

		static wchar_t tmp[512];

		// Print functions
		inline void print_integer(Object *obj, PrintDestination destination)
		{
			if(destination == PD_STDOUT)
			{
				printf("%d", obj->int_value);
			}
			else
			{
				memset(output_buffer, 0x00, 64);
				swprintf(output_buffer, L"%d", obj->int_value);
				OutputDebugStringW(LPCWSTR(output_buffer));
			}
		}

		inline void print_float(Object *obj, PrintDestination destination)
		{
			if(destination == PD_STDOUT)
			{
				printf("%f", obj->float_value);
			}
			else
			{
				memset(output_buffer, 0x00, 64);
				swprintf(output_buffer, L"%f", obj->float_value);
				OutputDebugStringW(LPCWSTR(output_buffer));
			}
		}

		inline void print_symbol(Object *obj, PrintDestination destination)
		{
			if(destination == PD_STDOUT)
			{
				printf("%s", obj->name);
			}
			else
			{
				memset(output_buffer_ansi, 0x00, 512);
				sprintf(output_buffer_ansi, "%s", obj->name);
				OutputDebugStringA(output_buffer_ansi);
			}
		}

		inline void print_string(Object *obj, PrintDestination destination)
		{
			if(destination == PD_STDOUT)
			{
				printf("\"%s\"", obj->str_value);
			}
			else
			{
				memset(tmp, 0x00, 512);
				mbstowcs(tmp, obj->str_value, 512);
				swprintf(output_buffer, L"%s", tmp);
				OutputDebugStringW(LPCWSTR(output_buffer));
			}
		}

		inline void print_character(Object *obj, PrintDestination destination)
		{
			if(destination == PD_STDOUT)
			{
				printf("#\\%c", obj->char_value);
			}
			else
			{
				memset(tmp, 0x00, 512);
				tmp[0] = obj->char_value;
				swprintf(output_buffer, L"#\\%c", tmp[0]);
				OutputDebugStringW(LPCWSTR(output_buffer));
			}
		}

		inline void print_array(Object *obj, PrintDestination destination)
		{
			if(destination == PD_STDOUT)
			{
				printf("#(");

				for(int i=0; i<obj->array_length; i++)
				{
					print(obj->array_contents[i], destination);
					printf(" ");
				}

				printf(")");
			}
			else
			{
				OutputDebugStringA("#(");

				for(int i=0; i<obj->array_length; i++)
				{
					print(obj->array_contents[i], destination);
					OutputDebugStringA(" ");
				}

				OutputDebugString(")");
			}
		}

		inline void print_cell(Object *obj, PrintDestination destination)
		{
			if(destination == PD_STDOUT)
			{
				printf("(");
				for (;;) {
					print(obj->car, PD_STDOUT);
					if (obj->cdr == Nil)
						break;
					if (obj->cdr->tag != T_CELL) {
						printf(" . ");
						print(obj->cdr, PD_STDOUT);
						break;
					}
					printf(" ");
					obj = obj->cdr;
				}
				printf(")");
			}
			else
			{
				OutputDebugStringA("(");
				for (;;) {
					print(obj->car, PD_DEBUGOUTPUT);
					if (obj->cdr == Nil)
						break;
					if (obj->cdr->tag != T_CELL) {
						OutputDebugStringA(" . ");
						print(obj->cdr, PD_DEBUGOUTPUT);
						break;
					}
					OutputDebugStringA(" ");
					obj = obj->cdr;
				}
				OutputDebugStringA(")");
			}
		}

		inline void print_primitive(Object *obj, PrintDestination destination)
		{
			if(destination == PD_STDOUT)
			{
				printf("<primitive>");
			}
			else
			{
				swprintf(output_buffer,  L"<primitive>");
				OutputDebugStringW(LPCWSTR(output_buffer));
			}
		}

		inline void print_function(Object *obj, PrintDestination destination)
		{
			if(destination == PD_STDOUT)
			{
				printf("<function>");
			}
			else
			{
				swprintf(output_buffer,  L"<function>");
				OutputDebugStringW(LPCWSTR(output_buffer));
			}
		}

		inline void print_macro(Object *obj, PrintDestination destination)
		{
			if(destination == PD_STDOUT)
			{
				printf("<macro>");
			}
			else
			{
				swprintf(output_buffer,  L"<macro>");
				OutputDebugStringW(LPCWSTR(output_buffer));
			}
		}

		inline void print_special(Object *obj, PrintDestination destination)
		{
			if(destination == PD_STDOUT)
			{
				if (obj == Nil)
					printf("()");
				else if (obj == True)
					printf("t");
				else
					error("Bug: print: Unknown special subtype: %d", obj->special_subtype);
			}
			else
			{
				if (obj == Nil)
				{
					swprintf(output_buffer, L"()");
					OutputDebugStringW(LPCWSTR(output_buffer));
				}
				else if (obj == True)
				{
					swprintf(output_buffer, L"t");
					OutputDebugStringW(LPCWSTR(output_buffer));
				}
				else
					error("Bug: print: Unknown subtype: %d", obj->special_subtype);
			}
		}

	}

}