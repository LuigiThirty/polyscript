#pragma once

#include <cassert>
#include <cctype>
#include <cstdio>
#include <string>
#include <cstdlib>
#include <cstdarg>

#include "Object.h"

namespace PolyScript
{
	void __declspec(dllexport) Initialize();
	void __declspec(dllexport) EvaluateString(const char *line);
	void __declspec(dllexport) EvaluateFile(const char *path);
		
	extern char string_under_evaluation[65536];
	extern int string_pointer;
	extern bool evaluating_a_script;
	extern bool end_of_script_flag;
	
}

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the POLYSCRIPT_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// POLYSCRIPT_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef POLYSCRIPT_EXPORTS
#define POLYSCRIPT_API __declspec(dllexport)
#else
#define POLYSCRIPT_API __declspec(dllimport)
#endif

// This class is exported from the PolyScript.dll
class POLYSCRIPT_API CPolyScript {
public:
	CPolyScript(void);
	// TODO: add your methods here.
};

extern POLYSCRIPT_API int nPolyScript;

POLYSCRIPT_API int fnPolyScript(void);

