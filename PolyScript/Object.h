#pragma once

#include <cassert>
#include <cctype>
#include <cstdio>
#include <string>
#include <cstdlib>
#include <cstdarg>

namespace PolyScript
{
	extern void error(const char *fmt, ...);
	extern bool error_flag;

	extern struct Object *obarray;

	extern struct Object *Nil;
	extern struct Object *Dot;
	extern struct Object *Cparen;
	extern struct Object *True;
	extern struct Object *Backtick;
	extern struct Object *Comma;

	extern struct Object *env;

	typedef struct Object *Primitive(struct Object *env, struct Object *args);

	typedef enum ObjectTag {
		T_ATOM = 0x0001,
		T_CELL,
		T_PRIMITIVE,
		T_FUNCTION,
		T_MACRO,
		T_ENV,
		T_SPECIAL,
	} ObjectTag;

	typedef enum AtomSubtype {
		AT_INT = 0x1001,
		AT_FLOAT,
		AT_SYMBOL,
		AT_STRING,
		AT_CHARACTER,
		AT_ARRAY,
	} AtomSubtype;

	// Subtypes for TSPECIAL
	typedef enum {
		T_NIL = 0x2001,
		T_DOT,
		T_CPAREN,
		T_TRUE,
		T_BACKTICK,
		T_COMMA
	} SpecialSubtype;


	// A Lisp object. Its contents depend on the union type.
	typedef struct Object {
		ObjectTag tag;

		// If an ATOM, it has a subtype
		AtomSubtype atom_subtype;

		// The size of this Object.
		size_t size;

		// The possible values of an Object.
		union
		{
			// AT_INT
			int int_value;

			// AT_FLOAT
			double float_value;

			// T_CELL
			struct {
				struct Object *car;
				struct Object *cdr;
			};

			// AT_SYMBOL
			char *name;

			// AT_STRING
			char *str_value;

			// AT_CHARACTER
			char char_value;

			// AT_ARRAY
			struct {
				// An array is implemented as an array of Objects.
				struct Object **array_contents;	// Pointer to the array of Objects.
				int array_length;				// Length of the array.
			};

			// T_PRIMITIVE
			Primitive *fn;

			// T_SPECIAL
			int special_subtype;

			// T_FUNCTION or T_MACRO
			struct {
				struct Object *params;
				struct Object *body;
				struct Object *env;
			};

			// T_ENV
			struct {
				struct Object *vars;
				struct Object *up;
			};
		};

		bool IsAtomSubtype(AtomSubtype subtype)
		{
			if (tag == T_ATOM && this->atom_subtype == subtype)
				return true;
			else
				return false;
		}

		bool IsSequence()
		{
			if (tag == T_ATOM && (this->atom_subtype == AT_STRING || this->atom_subtype == AT_ARRAY))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		bool IsNumber()
		{
			return tag == T_ATOM && (atom_subtype == AT_INT || atom_subtype == AT_FLOAT);
		}

		bool IsSymbol()
		{
			return tag == T_ATOM && atom_subtype == AT_SYMBOL;
		}

		void CopyData(Object *value)
		{
			this->tag = value->tag;
			this->size = value->size;
			this->atom_subtype = value->atom_subtype;

			switch(this->tag)
			{
			case T_ATOM:
				switch(this->atom_subtype)
				{
				case AT_INT:
					this->int_value = value->int_value;
					break;
				case AT_FLOAT:
					this->float_value = value->float_value;
					break;
				case AT_STRING:
					this->str_value = value->str_value;
					break;
				case AT_SYMBOL:
					this->name = value->name;
					break;
				case AT_CHARACTER:
					this->char_value = value->char_value;
					break;
				case AT_ARRAY:
					this->array_contents = value->array_contents;
					this->array_length = value->array_length;
					break;
				}
				break;
			case T_CELL:
				this->car = value->car;
				this->cdr = value->cdr;
				break;
			case T_PRIMITIVE:
				this->fn = value->fn;
				break;
			case T_SPECIAL:
				this->special_subtype = value->special_subtype;
				break;
			case T_FUNCTION:
			case T_MACRO:
				this->params = value->params;
				this->body = value->body;
				this->env = value->env;
				break;
			case T_ENV:
				this->vars = value->vars;
				this->up = value->up;
				break;
			default:
				error("Unknown type in Object::Copy");
				break;
			}
		}

		// Allocate a new Object.
		static Object *alloc(ObjectTag type, size_t size)
		{
			size += offsetof(Object, int_value);

			// Allocate an object.
			Object *obj = (Object *)malloc(size);
			obj->tag = type;
			obj->size = size;

			return obj;
		}

		// Constructors.
		static Object *MakeArray(int length)
		{
			Object *r = alloc(T_ATOM, sizeof(Object **) + sizeof(int));
			r->atom_subtype = AT_ARRAY;
			
			Object **array_ptr = (Object **)malloc(sizeof(Object *) * length);
			r->array_contents = array_ptr;
			r->array_length = length;

			return r;
		}

		static Object *MakeInt(int value)
		{
			Object *r = alloc(T_ATOM, sizeof(int));
			r->atom_subtype = AT_INT;
			r->int_value = value;
			return r;
		}

		static Object *MakeFloat(double value)
		{
			Object *r = alloc(T_ATOM, sizeof(double));
			r->atom_subtype = AT_FLOAT;
			r->float_value = value;
			return r;
		}

		static Object *MakeCharacter(const char c)
		{
			Object *r = alloc(T_ATOM, sizeof(char));
			r->atom_subtype = AT_CHARACTER;
			r->char_value = c;
			return r;
		}

		static Object *MakeString(const char *str)
		{
			Object *r = alloc(T_ATOM, sizeof(double));
			r->atom_subtype = AT_STRING;
			
			char *dup = _strdup(str);
			r->str_value = dup;

			return r;
		}

		static Object *MakeSymbol(const char *name) {
			Object *sym = alloc(T_ATOM, strlen(name) + 1);
			sym->atom_subtype = AT_SYMBOL;

			char *dup = _strdup(name);
			dup = _strupr(dup);
			sym->name = dup;

			return sym;
		}

		static Object *MakeFunction(ObjectTag type, Object *params, Object *body, Object *env) {
			assert(type == T_FUNCTION || type == T_MACRO);
			Object *r = alloc(type, sizeof(Object *) * 3);
			r->params = params;
			r->body = body;
			r->env = env;
			return r;
		}

		static Object *MakePrimitive(Primitive *fn) {
			Object *r = alloc(T_PRIMITIVE, sizeof(Primitive *));
			r->fn = fn;
			return r;
		}

		static Object *MakeEnv(Object *vars, Object *up) {
			Object *r = alloc(T_ENV, sizeof(Object *) * 2);
			r->vars = vars;
			r->up = up;
			return r;
		}

		static Object *MakeSpecial(SpecialSubtype subtype) {
			Object *r = (Object *)malloc(sizeof(void *) * 2);
			r->tag = T_SPECIAL;
			r->special_subtype = subtype;
			return r;
		}

		// By convention, this one is just called "cons"
		static Object *cons(Object *car, Object *cdr)
		{
			Object *cell = alloc(T_CELL, sizeof(Object *) * 2);
			cell->car = car;
			cell->cdr = cdr;

			return cell;
		}

		// Returns ((x . y) . a)
		static Object *acons(Object *x, Object *y, Object *a) {
			return cons(cons(x, y), a);
		}

		// May create a new symbol. If there's a symbol with the same name, it will not create a new symbol
		// but return the existing one.
		static Object *intern(const char *name) {
			char *tmp = _strdup(name);
			tmp = _strupr(tmp);

			for (Object *p = obarray; p != Nil; p = p->cdr)
				if (strcmp(tmp, p->car->name) == 0)
					return p->car;
			Object *sym = Object::MakeSymbol(tmp);
			obarray = Object::cons(sym, obarray);
			return sym;
		}

		Object * get_type_symbol()
		{
			switch(this->tag)
			{
				case T_ATOM:
					switch(this->atom_subtype)
					{
					case AT_INT:
						return intern("INTEGER");
					case AT_FLOAT:
						return intern("FLOAT");
					case AT_SYMBOL:
						return intern("SYMBOL");
					case AT_STRING:
						return intern("STRING");
					case AT_CHARACTER:
						return intern("CHARACTER");
					case AT_ARRAY:
						return intern("ARRAY");
					}
				case T_CELL:
					return intern("CONS");
				case T_PRIMITIVE:
				case T_FUNCTION:
				case T_MACRO:
					return intern("FUNCTION");
				case T_ENV:
					return intern("ENV");
				case T_SPECIAL:
					switch(this->special_subtype)
					{
					case T_NIL:
						return intern("NIL");
					case T_TRUE:
						return intern("T");
					case T_DOT:
						return intern("ERROR-DOT");
					case T_CPAREN:
						return intern("ERROR-CPAREN");
					}
			}
		}

		~Object()
		{
			if(tag == T_ATOM && IsSequence())
			{
				switch(atom_subtype)
				{
				case AT_STRING:
					free(str_value);
					break;
				case AT_ARRAY:
					//free(array_ptr);
					//break;
					break;
				default:
					break;
				}
			}
		}

	} Object;

};
