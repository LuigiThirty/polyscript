#pragma once

#include "PolyScript.h"

#include <cstdlib>
#include <string>

#include <Windows.h>

namespace PolyScript
{
	namespace Parser
	{
#define SYMBOL_MAX_LEN 200
#define NUMBER_MAX_LEN 100
#define STRING_MAX_LEN 2048

		typedef enum {
			PD_STDOUT,
			PD_DEBUGOUTPUT
		} PrintDestination;

		int peek();
		void skip_line();

		char get_next_char();

		Object *read_something(char c, bool in_backtick);
		Object * read_string();
		Object * read_numeric_string(char c);
		Object * read_symbol(char c);
		Object * read_quote();
		Object * read_list(bool in_backtick);
		Object * read(bool in_backtick);
		void print(Object *obj, PrintDestination destination);
	};
};

